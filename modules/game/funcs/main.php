<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_IS_MOD_GAME')) {
    die('Stop!!!');
}

$page_title = $module_info['site_title'];
$key_words = $module_info['keywords'];

$array_data = [];
$array_data = $db->query('SELECT * FROM nv4_game_manager WHERE status = 1')->fetchAll();

$template = array_column($db->query('SELECT config_value, config_name FROM '. $db_config['prefix'] . '_config WHERE module= "game" AND (config_name = "template" || config_name = "cover")')->fetchAll(), 'config_value', 'config_name');

//------------------
// Viết code vào đây
//------------------

$contents = nv_theme_game_main($array_data, $template);

include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents, 0);
include NV_ROOTDIR . '/includes/footer.php';
