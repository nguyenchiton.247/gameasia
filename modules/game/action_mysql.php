<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

$sql_drop_module = [];
$sql_drop_module[] = "DROP TABLE IF EXISTS " . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_manager";

$sql_create_module = $sql_drop_module;
$sql_create_module[] = "CREATE TABLE " . $db_config['prefix'] . "_" . $lang . "_" . $module_data . "_manager (
CREATE TABLE gameasia.nv4_game_manager ( id INT NOT NULL AUTO_INCREMENT , image TEXT NOT NULL DEFAULT '' , name VARCHAR(255) NOT NULL DEFAULT '' , link_android VARCHAR(255) NOT NULL DEFAULT '' , link_ios VARCHAR(255) NOT NULL DEFAULT '' , link_microsoft VARCHAR(255) NOT NULL DEFAULT '' , status TINYINT NOT NULL DEFAULT '1' , PRIMARY KEY (id)) ENGINE = InnoDB;
) ENGINE=MyISAM;";
