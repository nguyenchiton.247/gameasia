<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_IS_MOD_GAME')) {
    die('Stop!!!');
}

/**
 * nv_theme_game_main()
 * 
 * @param mixed $array_data
 * @return
 */
function nv_theme_game_main($array_data, $template)
{
    global $module_info, $lang_module, $lang_global;

    $xtpl = new XTemplate('main_4.tpl', NV_ROOTDIR . '/themes/' . $module_info['template'] . '/modules/' . $module_info['module_theme']);
    $xtpl->assign('LANG', $lang_module);
    $xtpl->assign('GLANG', $lang_global);

    $xtpl->assign('COVER', $template['cover']);
    if ($template['template'] != 1 ){
        $xtpl->assign('TCOL', (24/$template['template']));
        $xtpl->parse('main.style');
        if ($template['template'] == 2){
            $xtpl->assign('PER', 70);
        }else{
            $xtpl->assign('PER', 100);
        }
    }else{
        $xtpl->assign('TCOL', 22);
    }

    foreach ($array_data as $view) {
        $xtpl->assign('VIEW', $view);
        $xtpl->parse('main.loop');
    }

    $xtpl->parse('main');
    return $xtpl->text('main');
}

/**
 * nv_theme_game_detail()
 * 
 * @param mixed $array_data
 * @return
 */
function nv_theme_game_detail($array_data)
{
    global $module_info, $lang_module, $lang_global, $op;

    $xtpl = new XTemplate($op . '.tpl', NV_ROOTDIR . '/themes/' . $module_info['template'] . '/modules/' . $module_info['module_theme']);
    $xtpl->assign('LANG', $lang_module);
    $xtpl->assign('GLANG', $lang_global);

    //------------------
    // Viết code vào đây
    //------------------

    $xtpl->parse('main');
    return $xtpl->text('main');
}

/**
 * nv_theme_game_search()
 * 
 * @param mixed $array_data
 * @return
 */
function nv_theme_game_search($array_data)
{
    global $module_info, $lang_module, $lang_global, $op;

    $xtpl = new XTemplate($op . '.tpl', NV_ROOTDIR . '/themes/' . $module_info['template'] . '/modules/' . $module_info['module_theme']);
    $xtpl->assign('LANG', $lang_module);
    $xtpl->assign('GLANG', $lang_global);

    //------------------
    // Viết code vào đây
    //------------------

    $xtpl->parse('main');
    return $xtpl->text('main');
}
