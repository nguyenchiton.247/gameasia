<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_ADMIN') or !defined('NV_MAINFILE') or !defined('NV_IS_MODADMIN')) {
    die('Stop!!!');
}

define('NV_IS_FILE_ADMIN', true);

$allow_func = ['main', 'config', 'manager'];
