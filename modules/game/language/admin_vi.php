<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

$lang_translator['author'] = 'NguyenChiTon (nguyenchiton.247@gmail.com)';
$lang_translator['createdate'] = '22/03/2022, 10:15';
$lang_translator['copyright'] = '@Copyright (C) 2022 NguyenChiTon All rights reserved';
$lang_translator['info'] = '';
$lang_translator['langtype'] = 'lang_module';

$lang_module['main'] = 'Trang chính';
$lang_module['config'] = 'Cấu hình';
$lang_module['save'] = 'Lưu lại';

//Lang for function manager
$lang_module['manager'] = 'manager';
$lang_module['add'] = 'Thêm mới';
$lang_module['edit'] = 'Sửa';
$lang_module['delete'] = 'Xóa';
$lang_module['number'] = 'STT';
$lang_module['active'] = 'Trạng thái';
$lang_module['image'] = 'Image';
$lang_module['name'] = 'Name';
$lang_module['link_android'] = 'Link android';
$lang_module['link_ios'] = 'Link ios';
$lang_module['link_microsoft'] = 'Link microsoft';
$lang_module['status'] = 'Status';
$lang_module['error_required_image'] = 'Lỗi: bạn cần nhập dữ liệu cho Image';
$lang_module['error_required_name'] = 'Lỗi: bạn cần nhập dữ liệu cho Name';
$lang_module['error_required_link_android'] = 'Lỗi: bạn cần nhập dữ liệu cho Link android';
$lang_module['error_required_link_ios'] = 'Lỗi: bạn cần nhập dữ liệu cho Link ios';
$lang_module['error_required_link_microsoft'] = 'Lỗi: bạn cần nhập dữ liệu cho Link microsoft';
$lang_module['error_required_status'] = 'Lỗi: bạn cần nhập dữ liệu cho Status';

//Lang for function manager
$lang_module['search_title'] = 'Nhập từ khóa tìm kiếm';
$lang_module['search_submit'] = 'Tìm kiếm';

//Lang for function manager
$lang_module['error_url_link_android'] = 'Lỗi: url \' . $lang_module[\'link_android\'] không đúng';
$lang_module['error_url_link_ios'] = 'Lỗi: url \' . $lang_module[\'link_ios\'] không đúng';
$lang_module['error_url_link_microsoft'] = 'Lỗi: url \' . $lang_module[\'link_microsoft\'] không đúng';
