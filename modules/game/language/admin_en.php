<?php

/**
* @Project NUKEVIET 4.x
* @Author VINADES.,JSC <contact@vinades.vn>
* @Copyright (C) 2022 VINADES.,JSC. All rights reserved
* @Language English
* @License CC BY-SA (http://creativecommons.org/licenses/by-sa/4.0/)
* @Createdate Mar 22, 2022, 10:18:11 AM
*/

if (!defined('NV_ADMIN') or !defined('NV_MAINFILE'))
    die('Stop!!!');

$lang_translator['author'] = 'VINADES.,JSC (contact@vinades.vn)';
$lang_translator['createdate'] = '22/03/2022, 17:18';
$lang_translator['copyright'] = 'Copyright (C) 2022 VINADES.,JSC. All rights reserved';
$lang_translator['info'] = '';
$lang_translator['langtype'] = 'lang_module';



//Lang for function manager
$lang_module['manager'] = 'manager';
$lang_module['edit'] = 'edit';
$lang_module['delete'] = 'Delete';
$lang_module['number'] = 'Number';
$lang_module['active'] = 'Trạng thái';
$lang_module['image'] = 'Image';
$lang_module['name'] = 'Name';
$lang_module['link_android'] = 'Link android';
$lang_module['link_ios'] = 'Link ios';
$lang_module['link_microsoft'] = 'Link microsoft';
$lang_module['status'] = 'Status';
$lang_module['error_required_image'] = 'Error: Required fields enter the Image';
$lang_module['error_required_name'] = 'Error: Required fields enter the Name';
$lang_module['error_required_link_android'] = 'Error: Required fields enter the Link android';
$lang_module['error_required_link_ios'] = 'Error: Required fields enter the Link ios';
$lang_module['error_required_link_microsoft'] = 'Error: Required fields enter the Link microsoft';
$lang_module['error_required_status'] = 'Error: Required fields enter the Status';
$lang_module['save'] = 'Save';

//Lang for function manager
$lang_module['search_title'] = 'Enter keywords searching';
$lang_module['search_submit'] = 'Search';

//Lang for function manager
$lang_module['error_url_link_android'] = 'Error: Url \' . $lang_module[\'link_android\']';
$lang_module['error_url_link_ios'] = 'Error: Url \' . $lang_module[\'link_ios\']';
$lang_module['error_url_link_microsoft'] = 'Error: Url \' . $lang_module[\'link_microsoft\']';
