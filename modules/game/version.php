<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_MAINFILE')) {
    die('Stop!!!');
}

$module_version = [
    'name' => 'Game',
    'modfuncs' => 'main,detail,search',
    'change_alias' => 'main,detail,search',
    'submenu' => 'main,detail,search',
    'is_sysmod' => 0,
    'virtual' => 1,
    'version' => '4.3.03',
    'date' => 'Tue, 22 Mar 2022 10:15:17 GMT',
    'author' => 'NguyenChiTon (nguyenchiton.247@gmail.com)',
    'uploads_dir' => [$module_name],
    'note' => ''
];
