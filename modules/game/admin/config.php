<?php

/**
 * @Project NUKEVIET 4.x
 * @Author NguyenChiTon <nguyenchiton.247@gmail.com>
 * @Copyright (C) 2022 NguyenChiTon. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Tue, 22 Mar 2022 10:15:17 GMT
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    die('Stop!!!');
}

$page_title = $lang_module['config'];

if ($nv_Request->isset_request('submit', 'post')) {
    $row['template'] = $nv_Request->get_int('template', 'post', 1);

    $row['image'] = $nv_Request->get_title('image', 'post', '');
    if (!is_file(NV_DOCUMENT_ROOT . $row['image']))     {
        $row['image'] = '';
    }else{
        $stmt = $db->prepare('UPDATE ' . $db_config['prefix'] . '_config SET  config_value = :image WHERE module= "game" AND config_name = "cover"');
        $stmt->bindParam(':image', $row['image'], PDO::PARAM_INT);
        $exc = $stmt->execute();
    }

    $stmt = $db->prepare('UPDATE ' . $db_config['prefix'] . '_config SET  config_value = :template WHERE module= "game" AND config_name = "template"');
    $stmt->bindParam(':template', $row['template'], PDO::PARAM_INT);
    $exc = $stmt->execute();


}else{
    $row = array_column($db->query('SELECT config_value, config_name FROM '. $db_config['prefix'] . '_config WHERE module= "game" AND (config_name = "template" || config_name = "cover")')->fetchAll(), 'config_value', 'config_name');
}

$array_tempalte = [
    1 => 1,
    2 => 2,
    3 => 3
];
$xtpl = new XTemplate('config.tpl', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
$xtpl->assign('LANG', $lang_module);
$xtpl->assign('NV_LANG_VARIABLE', NV_LANG_VARIABLE);
$xtpl->assign('NV_LANG_DATA', NV_LANG_DATA);
$xtpl->assign('NV_BASE_ADMINURL', NV_BASE_ADMINURL);
$xtpl->assign('NV_NAME_VARIABLE', NV_NAME_VARIABLE);
$xtpl->assign('NV_OP_VARIABLE', NV_OP_VARIABLE);
$xtpl->assign('MODULE_NAME', $module_name);
$xtpl->assign('OP', $op);
$xtpl->assign('ROW', $row);

foreach ($array_tempalte as $key => $title) {
    $xtpl->assign('OPTION', [
        'key' => $key,
        'title' => $title,
        'selected' => ($key == $row['template']) ? ' selected="selected"' : ''
    ]);
    $xtpl->parse('main.select_tempalte');
}

//-------------------------------
// Viết code xuất ra site vào đây
//-------------------------------

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
