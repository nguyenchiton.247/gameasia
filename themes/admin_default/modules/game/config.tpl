<!-- BEGIN: main -->
<div class="panel panel-default">
    <div class="panel-body">
        <form class="form-horizontal" action="/admin/index.php?language=vi&amp;nv=game&amp;op=config" method="post">
            <input type="hidden" name="id" value="0">
            <div class="form-group">
                <label class="col-sm-5 col-md-4 control-label"><strong>Template</strong></label>
                <div class="col-sm-8 col-md-8">
                    <select class="form-control" name="template">
                        <!-- BEGIN: select_tempalte -->
                        <option value="{OPTION.key}" {OPTION.selected}>{OPTION.title}</option>
                        <!-- END: select_tempalte -->
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 col-md-4 control-label"><strong>{LANG.image}</strong> <span class="red">(*)</span></label>
                <div class="col-sm-8 col-md-8">
                    <div class="input-group">
                        <input class="form-control" type="text" name="image" value="{ROW.cover}" id="id_image" />
                        <span class="input-group-btn">
                <button class="btn btn-default selectfile" type="button" >
                <em class="fa fa-folder-open-o fa-fix">&nbsp;</em>
            </button>
            </span>
                    </div>
                </div>
            </div>
            <div class="form-group" style="text-align: center"><input class="btn btn-primary" name="submit"
                                                                      type="submit" value="Save"></div>
        </form>
    </div>
</div>
<script type="text/javascript">
    //<![CDATA[
    $(".selectfile").click(function() {
        var area = "id_image";
        var path = "{NV_UPLOADS_DIR}/{MODULE_UPLOAD}";
        var currentpath = "{NV_UPLOADS_DIR}/{MODULE_UPLOAD}";
        var type = "image";
        nv_open_browse(script_name + "?" + nv_name_variable + "=upload&popup=1&area=" + area + "&path=" + path + "&type=" + type + "&currentpath=" + currentpath, "NVImg", 850, 420, "resizable=no,scrollbars=no,toolbar=no,location=no,status=no");
        return false;
    });
    //]]>
</script>
<!-- END: main -->