<!-- BEGIN: main -->
<!-- BEGIN: style -->
<style>
    .urldownload .img-thumbnail {
        background-image: url(/themes/default/images/game/gbg.png);
        background-repeat: no-repeat;
        background-color: black;
        border: 4px solid #EFE0BB;
        display: flex;
        justify-content: center;
        border-radius: 25px;
        position: absolute;
        left: -4%;
        top: -15%;
        overflow: hidden;
        height: 150px;
        width: 150px;
        object-fit: cover;
    }

    .headurl .title_about {
        font-size: 20px;
        color: #EFE0BB;
        margin-left: 140px;
        font-weight: 600 !important;
        letter-spacing: 0 !important;
        text-transform: uppercase;
    }

    .read_more {
        margin-left: 135px;
    }

    .read_more_about {
        margin: 0 0px;
    }

    .read_more_about img {
        width: 50px;
        margin: 5px 0;
    }

    @media (max-width: 1630px) {

        .text {
            top: 25%;
        }

        .read_more_about img {
            width: 45px;
        }

        .read_more_about {
            margin: 0 5px;
        }

        .headurl .title_about {
            margin-left: 125px;
        }

        .headurl {
            height: 115px;
        }

        .urldownload .img-thumbnail {
            width: 140px;
            height: 140px;
        }

        .read_more {
            margin-left: 115px;
        }
    }

    @media (max-width: 1100px) {

        .text {
            top: 16%;
        }

        .read_more_about img {
            width: 46px;
        }

        .read_more_about {
            margin: 0 2px;
        }

        .read_more {
            margin-left: 115px;
        }

        .headurl .title_about {
            font-size: 20px;
            margin-left: 125px;
        }

    }

    @media (max-width: 1024px) {

        .text {
            top: 20%;
        }

        .read_more {
            margin-left: 120px;
        }

        .read_more_about {
            margin: 2px;
        }

        .read_more_about img {
            width: 40px;
        }

        .headurl .title_about {
            font-size: 18px;
        }

        .wraper {
            width: 100% !important;
        }
    }

    @media (max-width: 768px) {
        .text {
            top: 3%;
            font-size: 25px;
        }
        .read_more_about {
            margin: 0 5px;
        }
    }

    @media (max-width: 640px) {
        .text {
            top: 4%;
            font-size: 25px;
        }
    }

    @media (max-width: 375px) {
        .text {
            top: 3%;
            font-size: 25px;
        }
    }
</style>
<!-- END: style -->
<div class="section-body">
    <section>
        <div class="row">
            <div class="nv-block-banners cover">
                <img alt="Banner1" src="{COVER}" style="opacity: 0.5;">
                <h1 class="text">GAMES</h1>
            </div>
            <div class="wraper" style="width: {PER}%">
                <div class="row">
                    <!-- BEGIN: loop -->
                    <div class="col-md-{TCOL}">
                        <div class="list-style-none blockcolor urldownload">
                            <div class="headurl">
                                <img src="{VIEW.image}"
                                     alt="" width="175px" height="175px"
                                     class="img-thumbnail block-img">
                                <div>
                                    <h6 title="{VIEW.name}" class="show title_about" href="#">{VIEW.name}</h6>
                                </div>
                                <div class="read_more text-center">
                                    <a href="{VIEW.link_android}" class="read_more_about"><img
                                                src="/themes/default/images/game/ani.png"></a>
                                    <a href="{VIEW.link_ios}" class="read_more_about"><img
                                                src="/themes/default/images/game/ios.png"></a>
                                    <a href="{VIEW.link_microsoft}" class="read_more_about"><img
                                                src="/themes/default/images/game/wii.png"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- BEGIN: loop -->
                </div>
            </div>
        </div>
    </section>
</div>
<!-- END: main -->